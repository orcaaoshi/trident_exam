<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.8
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2016 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Welcome extends Controller
{
        public function action_list()
        {
            $data = array();
            $data['envelope'] = Model_Envelope::find('all');
            return Response::forge(View::forge('welcome/list', $data));
        }
    
        public function get_upload()
        {
            $data = array();
            return Response::forge(View::forge('welcome/upload', $data));
        }
    
        public function post_upload()
        {
            $image = Input::file('image');
            if ($image['name'] != "") {
                //var_dump($image);
                // 画像の保存
                move_uploaded_file($image['tmp_name'], 'assets/img/'.$image['name']);
                
                // DBへの保存
                $model = Model_Envelope::forge();
                $model->image_path = 'assets/img/'.$image['name'];
                $model->upload_datetime = date('Y-m-d H:i:s');
                $model->save();
                
                Response::redirect('welcome/list');
            } else {
                Response::redirect('welcome/upload');
            }
        }


        
        /**
	 * The basic welcome message
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		return Response::forge(View::forge('welcome/index'));
	}

	/**
	 * A typical "Hello, Bob!" type example.  This uses a Presenter to
	 * show how to use them.
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_hello()
	{
		return Response::forge(Presenter::forge('welcome/hello'));
	}

	/**
	 * The 404 action for the application.
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_404()
	{
		return Response::forge(Presenter::forge('welcome/404'), 404);
	}
}
